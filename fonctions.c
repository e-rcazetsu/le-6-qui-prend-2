#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "fonctions.h"
#define TAILLE 104
int estHumain() {
    int reponse;
    printf("Entrez 0 pour la verification humaine : ");
    scanf("%d", &reponse);
    if (reponse == 0) {
        return 1;
    } else {
        return 0;
    }
}

int age() {
    int age;
    printf("Entrez votre âge : ");
    scanf("%d", &age);
    if (age > 9) {
        return 1;
    } else {
        return 0;
    }
}

joueur* initialiserJoueurs(int nbJoueurs, carte* paquetdecarte) {
    joueur* tabjoueur = malloc(sizeof(joueur) * nbJoueurs);
    
    for (int i = 0; i < nbJoueurs; i++) {
        tabjoueur[i].nom = malloc(sizeof(char) * 100);
        printf("Veuillez saisir un nom : ");
        scanf("%s", tabjoueur[i].nom);
        
        if (estHumain() && age()) {
            printf("Le joueur '%s' répond aux critères.\n", tabjoueur[i].nom);
        } else {
            printf("Le joueur '%s' ne répond pas aux critères. Vous ne pouvez pas jouer.\n", tabjoueur[i].nom);
            exit(0); // Arreter le programme
        }
        
        tabjoueur[i].main = malloc(sizeof(carte) * 11);
        tabjoueur[i].main[10].num = 0;
        tabjoueur[i].point = 0;
        distribuercarte(tabjoueur[i], paquetdecarte);
        trierMainJoueur(tabjoueur[i].main, 10);
    }
    
    return tabjoueur;
}

int nbJoueurs() {
    int nombreJoueurs;

    printf("Veuillez saisir le nombre de joueurs (2/10): ");
    while (scanf("%d", &nombreJoueurs) != 1 || nombreJoueurs < 2 || nombreJoueurs > 10) {
        while (getchar() != '\n'); // Pour eviter un spam si l'utilisateur entre un charactère;

        printf("Le nombre saisi n'est pas valide. Veuillez saisir un nombre entre 2 et 10.\n");
        printf("Veuillez saisir le nombre de joueurs (2/10): ");
    }

    return nombreJoueurs;
}

void affjoueurs(int nbJoueurs, joueur* tabjoueur) {
    printf("===============================\n");
    printf("     Liste des Joueurs\n");
    printf("===============================\n");
    printf("Joueur\t\tNom\n");
    printf("===============================\n");
    for (int i = 0; i < nbJoueurs; i++) {
        printf("%d\t\t%s\n", i + 1, tabjoueur[i].nom);
    }
    printf("===============================\n");
}

carte inicarte(carte c){
    if ( c.num%11==0 && c.num%10==5){
        c.tdb=7;
    }else if (c.num%10==0){
        c.tdb=3;
    }else if (c.num%11==0) {
        c.tdb=5;
    }else if (c.num%10==5){
        c.tdb=2;
    }else{
        c.tdb=1;
    }
    return c;
}

carte* initialiserpaquetdecartes(int taille){
    carte* paquetdecarte;
    paquetdecarte=malloc(sizeof(carte)*taille);
    for (int i=1; i<=taille; i++ ){
        paquetdecarte[i-1].num=i;
        paquetdecarte[i-1]=inicarte(paquetdecarte[i-1]);
    }
    return paquetdecarte;
}

void initialiserpaquetdecartes2(int taille, carte* paquetdecarte){
    for (int i=1; i<=taille; i++ ){
        paquetdecarte[i-1].num=i;
        paquetdecarte[i-1]=inicarte(paquetdecarte[i-1]);
    }
}

void distribuercarte(joueur j, carte* paquetdecarte){
    int p,v,carterestante=0;
    do {
        carterestante++;
    } while (paquetdecarte[carterestante].num != 0);
        srand(time(NULL));
        for (int i=0; i<10; i++){
            p=rand()%carterestante;
            j.main[i].num=paquetdecarte[p].num;
            j.main[i].tdb=paquetdecarte[p].tdb;
            carterestante=carterestante-1;
            for ( v=p; v<carterestante+1; v++){
                paquetdecarte[v].num=paquetdecarte[v+1].num;
                paquetdecarte[v].tdb=paquetdecarte[v+1].tdb;
            }
            paquetdecarte[v+1].num=0;
            paquetdecarte[v+1].tdb=0;
    }
}

void echanger(carte* i, carte* j){
    carte tmp;
    tmp=*i;
    *i=*j;
    *j=tmp;
}
void affichermainJoueur(joueur J) {
    printf("\n");
    for (int k = 0; k < 10; k++) {
        if (J.main[k].num != 0)
        printf("+---------+  ");
    }
    printf("\n");

    for (int i = 0; i < 10; i++) {
        if (J.main[i].num != 0) 
            printf("|   %3d   |  ", J.main[i].num);
        }
    printf("\n");

    for (int k = 0; k < 10; k++) {
        if (J.main[k].num != 0) 
        printf("|         |  ");
    }
    printf("\n");

    for (int i = 0; i < 10; i++) {
         if (J.main[i].num != 0) 
        printf("|TDB : %3d|  ", J.main[i].tdb);
    }
    printf("\n");

    for (int k = 0; k < 10; k++) {
         if (J.main[k].num != 0) 
        printf("|         |  ");
    }
    printf("\n");

    for (int k = 0; k < 10; k++) {
         if (J.main[k].num != 0) 
        printf("+---------+  ");
    }
    printf("\n\n");
}



void trierMainJoueur(carte* tab, int taille){
    int enDesordre=1;
    while(enDesordre){
        enDesordre=0;
        for(int j=0; j<taille-1; j++){
            if (tab[j].num>tab[j+1].num){
                echanger(&tab[j], &tab[j+1]);
                enDesordre=1;
            }
        }
    }
}


carte** initialiserTableau(){
    int tailleligne=4;
    int taillecolonne=6;
    carte** tabJeu;
    tabJeu=malloc(sizeof(carte*)*tailleligne);
    for (int i=0; i<taillecolonne; i++){
        tabJeu[i]=malloc(sizeof(carte)*taillecolonne);
    }
    for(int i=0;i<4;i++){
        tabJeu[i][5].num=0;
    }
    return tabJeu;
}

void tirer4cartes(carte* paquetdecarte,carte** tabJeu){
    int carterestante=0;
    int p,temp;
    do {
        carterestante++;
    } while (paquetdecarte[carterestante].num != 0);
    srand(time(NULL));
    for (int i=0; i<4; i++){
        p=rand()%carterestante;
        tabJeu[i][0].num=paquetdecarte[p].num;
        tabJeu[i][0].tdb=paquetdecarte[p].tdb;
        carterestante=carterestante-1;
        for (int j=p; j<carterestante+1; j++){
            paquetdecarte[j].num=paquetdecarte[j+1].num;
            paquetdecarte[j].tdb=paquetdecarte[j+1].tdb;
            temp=j;
        }
        paquetdecarte[temp].num=0;
        paquetdecarte[temp].tdb=0;
        for(int j=1;j<5;j++){
            tabJeu[i][j].num=0;
            tabJeu[i][j].tdb=0;
        }
    }
}


carte* choixCarte(joueur* J, int nombreJoueurs, carte** tabJeu) {
    carte* tabMain;
    int choix, nbcarte;
    int indexMain;
    tabMain = malloc(sizeof(carte) * 10);
    afficherPlateau(tabJeu);
    for (int i = 0; i < nombreJoueurs; i++) {
        affichermainJoueur(J[i]);  
        do {
            printf("%s, veuillez sélectionner une de vos cartes : ", J[i].nom);  
            while (scanf("%d", &choix) != 1) {
                printf("Caractère invalide. Veuillez saisir un nombre : ");
                while (getchar() != '\n');
            }       
            indexMain = 0;
            while (J[i].main[indexMain].num != 0 && J[i].main[indexMain].num != choix) {
                indexMain++;
            }    
            nbcarte = 0;
            while (J[i].main[nbcarte].num != 0) {
                nbcarte++;
            }     
        } while (indexMain >= nbcarte); 
        tabMain[i] = J[i].main[indexMain]; 
        for (int j = indexMain; j < 10; j++) {
            J[i].main[j] = J[i].main[j + 1];
        }}
    return tabMain;
}


void plusPetiteCarte (carte** tabJeu, carte* tabMain, int nbJoueur, joueur* tabjoueur){
    for (int i=0; i<nbJoueur; i++){
        int min=tabMain[0].num;
        int indexMin=0;
        for (int j=1; j<nbJoueur ;j++){
            if (min>tabMain[j].num){
                min=tabMain[j].num;
                indexMin=j;
            }
        }
        PlacerPlusPetite(tabJeu, tabMain[indexMin],tabjoueur[indexMin],tabjoueur,indexMin);
        tabMain[indexMin].num=105;
    }
}

int* IndexDerniereCarte(carte** tabJeu){
    int* index;
    index=malloc(sizeof(int)*4);
    for (int i=0; i<4; i++){
            int j=0;
            while (tabJeu[i][j].num != 0){
                index[i]=j;
                j++;
            }
    }
    return index;
}

void remplacerLigne(carte** tabJeu, carte choisie, joueur J, joueur* tabjoueur, int indexJoueur) {
    int numLigne;
    int somme = 0;
    afficherPlateau(tabJeu);
    printf("%s, Saisir la ligne que vous voulez remplacer : ", J.nom);
    while (scanf("%d", &numLigne) != 1 || numLigne < 0 || numLigne > 3) {
        printf("Numéro de ligne invalide. Veuillez saisir un nombre entre 0 et 3 : ");
        while (getchar() != '\n');
    }
    for (int j = 0; j < 5; j++) {
        if (tabJeu[numLigne][j].num != choisie.num) {
            somme += tabJeu[numLigne][j].tdb;
        }
    }  
    tabJeu[numLigne][0] = choisie;
    tabjoueur[indexJoueur].point += somme;
    for (int i = 1; i < 5; i++) {
        tabJeu[numLigne][i].num = 0;
        tabJeu[numLigne][i].tdb = 0;
    }
}



void FinDeLigne(carte** tabJeu, carte choisie, int indexMin, joueur j, joueur* tabjoueur, int indexJoueur){
    for (int i=0; i<5; i++){
        tabjoueur[indexJoueur].point+=tabJeu[indexMin][i].tdb;
        tabJeu[indexMin][i].num=0;
        tabJeu[indexMin][i].tdb=0;
    }
    tabJeu[indexMin][0]=choisie;
}

void PlacerPlusPetite (carte** tabJeu, carte choisie, joueur j, joueur* tabjoueur, int indexJoueur){
    int dif[4];
    for (int i=0; i<4; i++){
        dif[i]=choisie.num-(tabJeu[i][(IndexDerniereCarte(tabJeu))[i]].num);
    }
    int indexMin=-1;
    int min=106;
    for (int j=0; j<4; j++){
        if (dif[j] > 0 && dif[j]<min){
            min=dif[j];
            indexMin=j;
        }
    }
    if(indexMin==-1){
        remplacerLigne(tabJeu, choisie,j,tabjoueur,indexJoueur);
    }else if ((IndexDerniereCarte(tabJeu))[indexMin]>=4){
        FinDeLigne(tabJeu, choisie, indexMin, j, tabjoueur,indexJoueur);
    } else {
        tabJeu[indexMin][((IndexDerniereCarte(tabJeu))[indexMin])+1]=choisie;
    }
}

int obtenirJoueurPointsMinimum(int nbJoueur, joueur* tabjoueur) {
    int pointsMinimum = tabjoueur[0].point;
    int joueurPointsMinimum = 0;

    for (int i = 1; i < nbJoueur; i++) {
        if (tabjoueur[i].point < pointsMinimum) {
            pointsMinimum = tabjoueur[i].point;
            joueurPointsMinimum = i;
        }
    }
    return joueurPointsMinimum;
}

int gagne(int nbJoueur, joueur* tabjoueur, int pointArret) {
    for (int i = 0; i < nbJoueur; i++) {
        if (tabjoueur[i].point >= pointArret) {
            return 1; 
        }
    }
    return 0; 
}


void Manche(carte** tabJeu, carte* paquetdecarte, joueur* tabjoueur, int nbJoueur) {
    int nombreManches, pointArret;
    printf("Entrez le nombre de manches à jouer (entre 1 et 3) : ");
    while (scanf("%d", &nombreManches) != 1 || nombreManches < 1 || nombreManches > 3) {
        while (getchar() != '\n');
        printf("Le nombre saisi n'est pas valide. Veuillez saisir un nombre entre 1 et 3 : ");
    }
    while (getchar() != '\n');
    printf("Entrez le point d'arrêt minimal pour avoir un gagnant (nombre de têtes de bœuf) : ");
    while (scanf("%d", &pointArret) != 1 || pointArret < 1) {
        while (getchar() != '\n');
        printf("Le nombre saisi n'est pas valide. Veuillez saisir un nombre supérieur à 0 : ");
    }
    while (getchar() != '\n');

    int joueurGagnant = -1;

    for (int m = 1; m <= nombreManches; m++) {
        printf("\nManche %d\n", m);
        Tour2Jeu(tabJeu, paquetdecarte, tabjoueur, nbJoueur);
        initialiserpaquetdecartes2(TAILLE, paquetdecarte);
        for (int i = 0; i < nbJoueur; i++) {
            distribuercarte(tabjoueur[i], paquetdecarte);
            trierMainJoueur(tabjoueur[i].main, 10);
        }

        if (gagne(nbJoueur, tabjoueur, pointArret)) {
            joueurGagnant = obtenirJoueurPointsMinimum(nbJoueur, tabjoueur);
            break;
        }
    }

    if (joueurGagnant != -1) {
        printf("\nLe GAGANT est : %s !\n", tabjoueur[joueurGagnant].nom);
    } else {
        printf("\nAucun gagnant pour cette partie.\n");
    }
}



void Tour2Jeu (carte** tabJeu, carte* paquetdecarte, joueur* tabjoueur, int nbJoueur){
    carte* tabMain=malloc(sizeof(carte)*10);
    tirer4cartes(paquetdecarte,tabJeu);
    for (int i=0; i<10; i++){
        tabMain=choixCarte(tabjoueur, nbJoueur,tabJeu);
        plusPetiteCarte(tabJeu, tabMain, nbJoueur, tabjoueur);
        printf("\n");
        printf("===============================\n");
        printf("\npoints: ");
        for(int i=0;i<nbJoueur;i++){
            printf(" %s :%d | ",tabjoueur[i].nom,tabjoueur[i].point);
        }
        printf("\n");
    }
}
void afficherPlateau(carte** tabJeu) {
    printf("===============================\n");
    printf("      Tab du Jeu     \n");
    printf("===============================\n");
    for (int i = 0; i < 4; i++) {
        printf("Ligne %d:\n", i); // afficher le numéro de la ligne pour 
        for (int j = 0; j < 5; j++) {
            printf("+---------+  ");
        }
        printf("\n");
        for (int j = 0; j < 5; j++) {
            if (tabJeu[i][j].num != 0) {
                printf("|   %3d   |  ", tabJeu[i][j].num);
            } else {
                printf("|         |  ");
            }
        }
        printf("\n");
        for (int j = 0; j < 5; j++) {
            printf("|         |  ");
        }
        printf("\n");
        for (int j = 0; j < 5; j++) {
            if (tabJeu[i][j].tdb != 0) {
                printf("|TDB : %3d|  ", tabJeu[i][j].tdb);
            } else {
                printf("|         |  ");
            }
        }
        printf("\n");
        for (int j = 0; j < 5; j++) {
            printf("|         |  ");
        }
        printf("\n");
        for (int j = 0; j < 5; j++) {
            printf("+---------+  ");
        }
        printf("\n");
        printf("\n");
    }
    printf("===============================\n");
    printf("\n");
}

