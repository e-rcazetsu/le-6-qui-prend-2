#ifndef __FONCTIONS_H_
#define __FONCTIONS_H_
#define TAILLE 104


typedef struct {
    int num;
    int tdb;
} carte;

typedef struct {
    char* nom;
    carte* main;
    int point;
} joueur;



/* Auteur :   */
/* Date :    10 juin */
/* Résumé :  affiche les joueurs et leurs noms */
/* Entrée(s) : le nombre des joueurs et le tad des joueurs*/
void affjoueurs(int nbJoueurs, joueur* tabjoueur);
/* Auteur :  Yassir  */
/* Date :    10 juin */
/* Résumé :  Verifier si un joueur est humain */
/* Entrée(s) : le nombre 0 */
/* Sortie(s) :  retourne 0  */
int estHumain();
/* Auteur :  Raphael */
/* Date :    10 juin */
/* Résumé :  Vérifier l'age du joueur*/
/* Entrée(s) :  son age */
/* Sortie(s) :  retourne 1  */
int age();
/* Auteur :  Eygann */
/* Date : 12 Juin */
/* Résumé : Permet d'initialiser les joueurs et leurs noms et de verif l'age et s'il est humain*/
/* Entrée(s) : un nombre de joueur  */
/* Sortie(s) : retourne un tableau de nom de joueur   */
joueur* initialiserJoueurs(int nbJoueurs, carte* paquetdecarte);
/* Auteur : Théo */
/* Date : 12 Juin */
/* Résumé : Permet de vérifier si le nombre de joueurs entré et entre 2 et 10*/
/* Sortie(s) :  retourne un nombre de joueurs  */
int nbJoueurs ();
/* Auteur : Alexis */
/* Date :  12 Juin */
/* Résumé : Permet d'initialiser le nombre de tête de boeufs par rapport aux numéro de chaque carte */
/* Entrée(s) : prend une carte en entrée */
/* Sortie(s) :  renvoie une carte avec le nombre de tête de boeufs  */
carte inicarte(carte c);
/* Auteur : Yassir */
/* Date :   13 Juin */
/* Résumé : Permet d'initialiser un tableau 1D dynamique avec le numéro de toutes les cartes */
/* Entrée(s) : prend une taille qui vaut 104 en entrée */
/* Sortie(s) : renvoie un tableau 1D avec toutes les cartes du jeu (leur numéro) */
carte* initialiserpaquetdecartes(int taille);
/* Auteur : Raphael */
/* Date :   17 Juin */
/* Résumé : Permet d'initialiser le paquet de carte en décalant toutes les cartes de 1 rang vers la droite */
/* Entrée(s) : prend la taille du paquet de carte et le paquet de carte */
void initialiserpaquetdecartes2(int taille,carte* paquetdecarte);
/* Auteur : Eygann */
/* Date :  13 Juin */
/* Résumé : Permet de distribuer aléatoirement des cartes aux joueurs */
/* Entrée(s) : un joueur et un tableau 1D de cartes */
void distribuercarte(joueur j, carte* paquetdecarte);
/* Auteur : Théo */
/* Date : 14 Juin */
/* Résumé : Permet d'échanger 2 éléments pour le tri de la main du joueur */
/* Entrée(s) : 2 cartes */
void echanger(carte* i, carte* j);
/* Auteur : Alexis */
/* Date : 14 Juin */
/* Résumé : Permet de ranger dans l'ordre croissant les cartes par rappor à leur numéro */
/* Entrée(s) : prend en entrée un tableau de carte et la taille du tableau */
void trierMainJoueur(carte* tab, int taille);
/* Auteur : Yassir */
/* Date :  14 Juin */
/* Résumé : permet d'afficher sur une ligne les 10 cartes du joueurs avec leur numéro et le nombre de tête de boeuf */
/* Entrée(s) : prend en entrée un tableau de joueur et le nombre de carte que possède le joueur */
void affichermainJoueur (joueur J);
/* Auteur : Raphael */
/* Date :   14 Juin */
/* Résumé : Permet d'allouer un tableau 2D de cartes */
/* Sortie(s) : renvoie un tableau 2D de cartes  */
carte** initialiserTableau();
/* Auteur : Eygann */
/* Date :   14 Juin */
/* Résumé : Permet de tirer 4 cartes aléatoirement et de les ranger dans un tableau 1D */
/* Entrée(s) : prend en entré un tableau de carte qui correspond au paquet de cartes */
/* Sortie(s) :  renvoie un tableau 1D comportant les 4 cartes tirées aléatoirement  */
void tirer4cartes(carte* paquetdecarte,carte** tabJeu);
/* Auteur : Théo */
/* Date :   16 Juin */
/* Résumé : Permet aux Joueurs de choisir la carte qu'ils souhaitent jouer et la ranger dans un tableau 1D dynamique */
/* Entrée(s) : Prend en entrée un tableau de joueurs et un nombre de joueurs */
/* Sortie(s) : renvoi un tableau 1D avec les cartes jouées par les joueurs */
carte* choixCarte (joueur* J, int nombreJoueurs, carte** tabJeu);
/* Auteur : Alexis */
/* Date :   16 Juin */
/* Résumé : Permet de vérifier la plus petite carte entre les cartes choisies par les joueurs */
/* Entrée(s) : Prend en entrée un tableau de joueurs, un nombre de joueurs, un tableau 2D qui correspond au plateau de jeu et un tableau 1D de carte */
void plusPetiteCarte (carte** tabJeu, carte* tabMain, int nbJoueur, joueur* tabjoueur);
/* Auteur : Yassir */
/* Date :   16 Juin */
/* Résumé : Permet de renvoyer le numéro de la colonne où est située la dernière carte pour chaque ligne du plateau de jeu */
/* Entrée(s) : Prend en entrée le plateau de jeu */
/* Sortie(s) : renvoi un tableau d'entiers avec les index des 4 lignes */
int* IndexDerniereCarte(carte** tabJeu);
/* Auteur : Raphael */
/* Date :   16 Juin */
/* Résumé : Permet de remplacer une ligne du plateau de jeu lorsque la série est pleine (5 cartes) et permet d'attribuer au joueur le nombre de têtes de boeufs associé à la série remplacée */
/* Entrée(s) : Prend en entrée le plateau de jeu, la carte que veut placer le joueur et un joueur */
void remplacerLigne(carte** tabJeu, carte choisie, joueur J, joueur* tabjoueur, int indexJoueur);
/* Auteur : Eygann */
/* Date :   17 Juin */
/* Résumé : Permet de remplacer automatiquement une ligne lorsque un joueur joue une carte proche du numéro de la dernière carte de la ligne.
Cette procédure permet également d'attribuer les tête de boeufs au joueur qui récupère la ligne */
/* Entrée(s) : Prend en entrée le plateau de jeu, la carte que veut placer le joueur, un joueur, l'index de la ligne où est situé la carte avec le numéro le plus proche de celle du joueur,
un tableau de joueur et l'index d'un joueur */
void FinDeLigne(carte** tabJeu, carte choisie, int indexMin, joueur j, joueur* tabjoueur, int indexJoueur);
/* Auteur : Théo */
/* Date :   16 Juin */
/* Résumé : Permet de placer automatiquement lorque la série n'est pas pleine une carte après la carte avec le numéro le plus proche */
/* Entrée(s) : Prend en entrée le plateau de jeu, la carte choisie par le joueur et un joueur */
void PlacerPlusPetite (carte** tabJeu, carte choisie, joueur j, joueur* tabjoueur, int indexJoueur);
/* Auteur : Alexis */
/* Date :   16 Juin */
/* Résumé : Permet de vérifier si un joueur à atteint le nombre max de tête de boeufs pour terminer la partie */
/* Entrée(s) : Prend en entrée le nombre de joueur et un tableau de joueur */
/* Sortie(s) : renvoi un booléen pour savoir si le nombre max de tête de boeufs à ét atteint ou non*/
int gagne(int nbJoueur, joueur* tabjoueur, int pointArret);
/* Auteur : Yassir */
/* Date :   16 Juin */
/* Résumé : Permet de faire plusieures manches de jeu jusqu'à ce que la partie se termine */
/* Entrée(s) : Prend en entrée le nombre de joueur, le tableau de joueur, le paquet de cartes et le plateau de carte */
void Manche(carte** tabJeu, carte* paquetdecarte, joueur* tabjoueur, int nbJoueur);
/* Auteur : Raphael */
/* Date :   16 Juin */
/* Résumé : Permet de réaliser un tour de jeu où chaque joueur và jouer ses 10 cartes */
/* Entrée(s) : Prend en entrée le nombre de joueur, le tableau de joueur, le paquet de cartes et le plateau de carte */
void Tour2Jeu (carte** tabJeu, carte* paquetdecarte, joueur* tabjoueur, int nbJoueur);
/* Auteur : Eygann */
/* Date :   16 Juin */
/* Résumé : Permet d'afficher le plateau de jeu et les cartes jouées sur celui-ci */
/* Entrée(s) : Prend en entrée le plateau de jeu */
void afficherPlateau(carte** tabJeu);
/* Auteur :  Yassir   */
/* Date :   17 juin */
/* Résumé : Permet de savoir le joueur avec le moins nombre de points */
/* Entrée(s) : Prend en entrée les points des joueurs*/
int obtenirJoueurPointsMinimum(int nbJoueur, joueur* tabjoueur);

#endif
