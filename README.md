# Readme - Jeu du 6 qui prend en c

Ce readme fournit une brève description et des instructions pour éxecuter le code en C.
Le jeu de cartes apéro "6 qui prend !" est un jeu de Wolfgang Kramer qui peut être joué par 2 à 10 joueurs âgés de 10 ans et plus. Il comprend 104 cartes avec des numéros allant de 1 à 104 et des symboles représentant des têtes de bœufs, qui correspondent aux points de pénalité. Le but du jeu est de récolter le moins possible de têtes de bœufs. En fin de partie, le gagnant est celui qui a accumulé le moins de points de pénalité.
La durée d'une partie est généralement de 45 minutes.

## Prérequis 

Pour exécuter ce code, vous devez avoir un compilateur C installé sur votre machine.
Par exemple, vous pouvez utiliser GCC (GNU Compiler Collection )sur les systèmes basés sur Unix/Linux ou MinGW sur Windows.

## instructions d'exécution

1. Clonez ou téléchargez le dépot contenant les fichiers.c/.h main.c et le makefile
2. Ouvrez un terminal et accédez au répertoire contenat les fichier du jeu 
3. Utilisez la commande suivante pour compiler le code : make
Cela génerera un fichier exécutable appelé  'exe'
4. Exécutez le fichier exécutable généré en utilisant la commande suivante : ./exe
5. Mettez le terminal en mode plein écran pour avoir les cartes bien affichés

## Fonctionnalités du jeu 

le code fourni implémente les fonctionnalités de base du jeu du 6-qui-prend, y comppris :
-La création d'un jeu de cartes avec des valeurs allant de 1 à 104.
-Le mélange des cartes et la distribution des cartes aux joueurs.
-Le calcul du nombre de tetes de boeuf prises par chaque joueurs.
-L'annonce du gagnat.
-le code utilise des fonctions et des structures pour organiser le jeu et les joueurs.
 
-Et nous avons conçu notre programme de manière flexible pour offrir une expérience ludique.
-Les joueurs ont la possibilité de choisir les manches et les têtes de bœuf qu'ils souhaitent jouer.
-Ils peuvent ainsi sélectionner les manches de leur choix jusqu'à ce qu'un joueur atteigne le nombre de têtes de bœuf désiré.
-À ce moment-là, le jeu s'arrête et nous déterminons le gagnant en fonction des points accumulés. Le gagnant est celui qui a le moins de points parmi tous les joueurs. Si toutes les manches sont terminées et qu'aucun joueur n'atteint le nombre de points requis, il n'y a pas de gagnant.
-Enfin, pour rendre le code plus interactif, nous avons également ajouté une option permettant aux joueurs de décider s'ils souhaitent rejouer ou non.

##Avertissement 
-Vous ne pouvez pas afficher correctement les cartes des joueurs si vous ne mettez pas le terminal en mode plein écran !







