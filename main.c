#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <stdio.h>
#include "fonctions.h"

#define TAILLE 104

int main() {
    int choix;
        printf("=====================================================\n");
        printf("|                 ====================              |\n");
        printf("|                 {   6 QUI PREND !   }             |\n");
        printf("|                 { GARE AU VACHERIES }             |\n");
        printf("|                 ====================              |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                   !!! (MOUUUUUU) !!!              |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                 /\\                  /\\            |\n");
        printf("|                 ||----|--------|----||            |\n");
        printf("|                 ||----          ----||            |\n");
        printf("|                      |          |                 |\n");
        printf("|                      \\\\        //                 |\n");
        printf("|                       \\\\      //                  |\n");
        printf("|                        |______|                   |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("|                       {BONJOUR}                   |\n");
        printf("|                                                   |\n");
        printf("|            Voulez-vous jouer ? (o/n) :            |\n");
        printf("|                                                   |\n");
        printf("|                                                   |\n");
        printf("=====================================================\n");
        printf("                     -ALORS ? : \n");
            
         do {
        printf("Entrer 'o' pour commencer et 'n' pour sortir : ");
        char opt[100];  
        fgets(opt, sizeof(opt), stdin);
        char option = tolower(opt[0]); /// Transformer une majuscule a une miniscule 
        if (option == 'o') {
            printf("\n");
            printf("              {  Allons jouer !!!  }                  \n");
            printf("\n");

            int nbJoueur;
            int confirmerNoms = 0;
            carte* paquetdecarte;
            joueur* tabjoueur;

            do {
                nbJoueur = nbJoueurs();
                paquetdecarte = initialiserpaquetdecartes(TAILLE);
                tabjoueur = initialiserJoueurs(nbJoueur, paquetdecarte);
                affjoueurs(nbJoueur, tabjoueur);

                do {
                    printf("Sélectionnez une option :\n");
                    printf("1. Ré-sélectionner les joueurs\n");
                    printf("2. Confirmer les joueurs\n");
                    printf("3. Sortir\n");
                    printf("Option : ");
                    int optionconfirmation;

                    while (scanf("%d", &optionconfirmation) != 1 || optionconfirmation < 1 || optionconfirmation > 3) {
                        printf("Option invalide. Veuillez sélectionner une autre option : ");

                        while (getchar() != '\n');
                    }

                    switch (optionconfirmation) {
                        case 1:
                            // Ré-sélectionner le nombre de joueurs
                            break;
                        case 2:
                            // Confirmer les joueurs et continuer le jeu
                            confirmerNoms = 1;
                            printf("\n");
                            printf("===============================\n");
                            printf("Parfait, allons jouer !\n");
                            printf("\n");
                            break;
                        case 3:
                            printf("Au revoir !...\n");
                            return 0; // Quitter le jeu
                        default:
                            printf("Option invalide. Veuillez sélectionner une autre option.\n");
                    }
                } while (getchar() != '\n');

            } while (!confirmerNoms);

            carte** tabJeu = initialiserTableau();
            Manche(tabJeu, paquetdecarte, tabjoueur, nbJoueur);
        } else if (option == 'n') {
            printf("Au revoir...\n");
            break;
        } else {
            printf("Option invalide. Veuillez sélectionner de nouveau votre choix\n");
            printf("\n");
            continue;
        }

        printf("\nVoulez-vous rejouer ?\n");
        printf("1. Oui\n");
        printf("2. Non\n");
        printf("Votre choix : ");

        while (scanf("%d", &choix) != 1 || (choix != 1 && choix != 2)) {
            printf("Veuillez entrer 1 pour rejouer ou 2 pour quitter : ");
            while (getchar() != '\n');  
        }

        if (choix == 2) {
            printf("Au revoir...\n");
            break;
        }

        while (getchar() != '\n');

    } while (1);

    return 0;
}