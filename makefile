exe: fonctions.o main.o
	@echo "Compilation de l'éxécutable"
	gcc fonctions.o main.o -o exe

fonctions.o: fonctions.c fonctions.h
	@echo "Compilation du fichier fonctions"
	gcc -c -Wall fonctions.c -o fonctions.o

main.o: main.c fonctions.h
	@echo "Compilation du main"
	gcc -c -Wall main.c -o main.o

clean:
	@echo "Supprimer les .o"
	rm -f *.o
	clear
